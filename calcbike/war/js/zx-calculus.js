'use strict'
function Calculator( element ) {
	this.element = element;
	this.$element = $(element);
	this.init();
}


Calculator.prototype.init = function(){
	this.inputTop = 0;
	this.inputBottom = 0;
	this.memory = 0;
	this.$inputButtom = this.$element.find('.inputBottom');
	this.$inputTop = this.$element.find('.inputTop');
	this.$element.find('#clear').on('click', {parent:this}, this.clearClick);
	this.$element.find('#clearAll').on('click', {parent:this}, this.clearAllClick);
	this.$element.find('#clearSingle').on('click', {parent:this}, this.clearSingleClick);
	this.$element.find(".digitBlok p").on('click', {parent:this}, this.digitClick);
	this.$element.find("#plusOrMinus").on('click', {parent:this}, this.plusOrMinus);
	this.$element.find(".arefmeticSection p").on('click', {parent:this}, this.pressCalculate);
	this.$element.find("#equal").on('click', {parent:this}, this.pressEqual);
	this.$element.find("#memoryClear").on('click', {parent:this}, this.pressMemoryClear);
	this.$element.find("#memoryRead").on('click', {parent:this}, this.pressMemoryRead);
	this.$element.find("#memorySave").on('click', {parent:this}, this.pressMemorySave);
	this.$element.find("#memoryPlus").on('click', {parent:this}, this.pressMemoryPlus);
	this.$element.find("#memoryMinus").on('click', {parent:this}, this.pressMemoryMinus);
	this.$element.find("#sqrt").on('click', {parent:this}, this.pressSqrt);
	
};

Calculator.prototype.getInputBottomValue = function(){
	return this.$inputButtom.attr("value");
};

Calculator.prototype.appendToInputTopValue = function(value, operation){
	var str = this.$inputTop.val();
	if (str.charAt(0) === '0'){
		str = str.substr(1);
	}
	if (str == '0'){
		this.inputTop = value;
		str = str.concat(value);
	} else {
		str = str.concat(value);
		var bignumber = math.eval(str);
		this.inputTop = bignumber.toString();
		 
		str = str.concat(operation);
		
	}
	return this.$inputTop.attr("value", str);
};

Calculator.prototype.setInputBottomValue = function(inputValue){
	this.$inputButtom.attr("value", inputValue);
	this.inputButtom = inputValue;
};

Calculator.prototype.getInputTopValue = function(){
	return this.$inputTop.attr("value");
};

Calculator.prototype.setInputTopValue = function(inputValue){
	this.$inputTop.attr("value", inputValue);
	this.inputButtom = inputValue;
};

Calculator.prototype.digitClick = function(event){
	var that = event.data.parent;
	var digitButton = $(this).text();
	var inputValue = that.getInputBottomValue();
	if(inputValue == "0" && digitButton == "."){
		inputValue += digitButton;
	} else if (inputValue == "0"){
		inputValue = digitButton;
	} else if (inputValue.indexOf(".") !== -1 && digitButton == ".") {
		return;
	} else {
		inputValue = inputValue + digitButton;
	}
	that.setInputBottomValue(inputValue);
};

Calculator.prototype.clearSingleClick = function(event){
	var that = event.data.parent;
	var str = that.$inputButtom.attr("value").slice(0, -1);
	if (str == 0){
		that.setInputBottomValue(0);
	} else {
		that.setInputBottomValue(str);
	}
};

Calculator.prototype.clearClick = function(event){
	var that = event.data.parent;
	that.setInputBottomValue(0);
};

Calculator.prototype.clearAllClick = function(event){
	var that = event.data.parent;
	that.setInputBottomValue(0);
	that.setInputTopValue(0);
};

Calculator.prototype.plusOrMinus = function(event){
	var that = event.data.parent;
	that.setInputBottomValue(-that.getInputBottomValue());
};

Calculator.prototype.pressCalculate = function(event){
	var that = event.data.parent;
	var operation = $(this).text();
	that.appendToInputTopValue(that.getInputBottomValue(), operation);
	that.setInputBottomValue(0);
	if ((that.getInputTopValue()).length>36){
		that.$element.find('#indicatorLengthInputTop').text("<<");
	}
};

Calculator.prototype.pressEqual = function(event){
	var that = event.data.parent;
	that.appendToInputTopValue(that.getInputBottomValue(), '');
	that.setInputBottomValue(that.inputTop);
	that.setInputTopValue(0);
};

Calculator.prototype.pressMemoryClear = function(event){
	var that = event.data.parent;
	that.memory = 0;
	that.$element.find('#indicatorMemory').text("");
};

Calculator.prototype.pressMemoryRead = function(event){
	var that = event.data.parent;
	that.setInputBottomValue(that.memory);
};

Calculator.prototype.pressMemorySave = function(event){
	var that = event.data.parent;
	that.memory = that.getInputBottomValue();
	if (that.memory !== "0"){
		that.$element.find('#indicatorMemory').text("M");
	}
};

Calculator.prototype.pressMemoryPlus = function(event){
	var that = event.data.parent;
	that.memory = (+that.memory) + (+that.getInputBottomValue());
	
};

Calculator.prototype.pressMemoryMinus = function(event){
	var that = event.data.parent;
	that.memory = (+that.memory) - (+that.getInputBottomValue());
	
};

Calculator.prototype.pressSqrt = function(event){
	var that = event.data.parent;
	var x = Math.sqrt(that.getInputBottomValue());
	that.setInputBottomValue(x);
	
};
