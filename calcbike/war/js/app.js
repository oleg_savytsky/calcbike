'use strict'
math.config({
	  number: 'bignumber', // Default type of number: 
	                       // 'number' (default), 'bignumber', or 'fraction'
	  precision: 16        // Number of significant digits for BigNumbers
	});
var calcContainers = $('.calculus');
var template = $("#calcTemplate").html();
calcContainers.replaceWith(template);
calcContainers = $('.calculus');
calcContainers.each(function(){
	new Calculator(this);
});